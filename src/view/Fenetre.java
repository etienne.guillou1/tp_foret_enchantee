package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.Main;
import model.Agent;
import model.Case;
import model.Environment;
import model.UpdateEventListener;

public class Fenetre extends JFrame implements UpdateEventListener, ActionListener {
	
	private JPanel panel, panelButton;
	private JButton button;
	private JLabel[][] cases;
	private JButton btnLaunch;
	
	public Environment env;
	public Agent agent;
	
	public Fenetre(Environment env, Agent agent) {
		this.setTitle("TP3 - For�t enchant�e");
		this.setSize(805, 908);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    this.setVisible(true);
	    this.setResizable(true);
	    this.setLayout(new BorderLayout());
	    this.env = env;
	    this.agent = agent;
	    
	    new JPanel(new GridLayout(2, 1));
	    
	    panel = new JPanel(new GridLayout(env.getLevel(), env.getLevel()));
	    
	    btnLaunch = new JButton("Effectuer un d�placement");
	    btnLaunch.setPreferredSize(new Dimension(0, 75));
	    
	    
	    generateGrid();
	    
	    this.add(panel, BorderLayout.CENTER);
	    this.add(btnLaunch, BorderLayout.SOUTH);

	    // Inscription de l'environnement et de l'agent au listener
	    env.setUpdateEventListener(this);
	    agent.setUpdateEventListener(this);
	    btnLaunch.addActionListener(this);
	}
	
	public void generateGrid() {
		cases = new JLabel[env.getLevel()][env.getLevel()];
		int agentPosX = agent.getCoords().getX();
		int agentPosY = agent.getCoords().getY();
		System.out.println(agentPosX);
		
		for(int i = 0; i < env.getLevel(); i++) {
            for(int j = 0; j < env.getLevel(); j++) {
            	cases[i][j] = new JLabel(getImageIcon(env.getForest()[i][j]));
            	panel.add(cases[i][j]);
            }
		}
		cases[agentPosX][agentPosY].setIcon(new StretchIcon(Main.class.getResource("/resources/hero.png")));
	}
	
	public void refreshForest(Case[][] forest) {
		panel.removeAll();
		panel.setLayout((new GridLayout(env.getLevel(), env.getLevel())));
		generateGrid();
		panel.updateUI();
	}
	
	public static void displayForest(Environment environment) {
		for (int i=0; i<environment.getLevel(); i++) {
			for (int j=0; j<environment.getLevel(); j++) {
				System.out.print(environment.getForest()[i][j] + "   ");
			}
			System.out.println();
		}
	}
	
	private String getStringElement(Case caseContent) {
		String path = "";
		switch(caseContent) {
			case EMPTY: return "_";
			case WIND: return "Wind";
			case POOP: return "Poop";
			case CRACK: return "Crack";
			case MONSTER: return "Monster";
			case LIGHT: return "Light";
			case WINDANDPOOP: return "Wind & Poop";
			default: return "_";
		}
	}
	
	private StretchIcon getImageIcon(Case caseContent) {
		String path = "";
		switch(caseContent) {
			case EMPTY:
				path = "/resources/empty.png";
				break;
			case WIND:
				path = "/resources/wind.png";
				break;
			case POOP:
				path = "/resources/poop.png";
				break;
			case CRACK:
				path = "/resources/crack.png";
				break;
			case MONSTER:
				path = "/resources/monster.png";
				break;
			case LIGHT:
				path = "/resources/portail.png";
				break;
			case WINDANDPOOP:
				path = "/resources/windandpoop.png";
				break;
			case MONSTERANDWIND:
				path = "/resources/monster.png";
				break;
			case MONSTERANDPOOP:
				path = "/resources/monster.png";
				break;
			default: 
				path = "/resources/hero.png";
				break;
		}
		return new StretchIcon(Main.class.getResource(path));
	}
	
	/**
	 * Fonction qui permet d'actualiser l'affichage de la fenetre
	 * @param map
	 * @param updateAgent
	 * @throws IOException
	 */
	public void refresh(Case[][] forest) throws IOException {
        
		for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
            	cases[i][j].setIcon(getImageIcon(forest[i][j]));
            }
        }
	}

	@Override
	public void onUpdateEnv() {
		System.out.println("refresh env");
		refreshForest(env.getForest());
	}

	@Override
	public void onUpdateAgent() {
		System.out.println("refresh agent");
		refreshForest(env.getForest());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnLaunch) {
			agent.jouer();
		}
	}

}
