package controller;

import java.util.ArrayList;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.Action;
import model.Agent;
import model.But;
import model.Coords;
import model.Environment;
import model.Etat;
import model.Fait;
import model.MoteurInference;
import model.Regle;
import view.Fenetre;

public class Main {

	public static Fenetre f;
	public static void main(String[] args) {

		f = null;
		

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		java.awt.EventQueue.invokeLater(new Runnable() {
	        public void run() {
	        	Environment env = new Environment(3);
	    		Fenetre.displayForest(env);
	    		
	    		Agent agent = new Agent(env);
	    		//agent.start();
	        	f = new Fenetre(env, agent);
	        }
	    });
//		
//		for(int i = 4; i < 10; i++) {
//			try {
//				Thread.sleep(2000);
//				//environment.setLevel(i);
//				//environment.updateForest();
//				//environment.update();
//				agent.activeEffecteurs(Action.DROITE);
//				agent.update();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
		

//		MoteurInference.lectureFaits();
		
//		m.displayRegles();

	}

}
