package model;

import java.util.ArrayList;
/**
 * Classe qui permet de conaitre les coordonnees d'une case 
 */
public class Coords {
	
	private int x; 
	private int y; 
	
	public Coords(int x, int y) {
		this.x = x; 
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "Coords [x=" + x + ", y=" + y + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Coords other = (Coords) obj;
		
		if (x != other.getX())
			return false;
		if (y != other.getY())
			return false;
		return true;
	}
	
	/**
	 * Fonction qui permet d'avoir la liste des voisins d'une case (ceux qui sont dans la foret)
	 * @param env
	 * @return
	 */
	public ArrayList<Coords> neighbors(Environment env) {
		ArrayList<Coords> neighbors = new ArrayList<Coords>();
		if (env.inForest(this.x-1,this.y))
			neighbors.add(new Coords(this.x-1, this.y));
		if (env.inForest(this.x+1,this.y))
			neighbors.add(new Coords(this.x+1, this.y));
		if (env.inForest(this.x,this.y-1))
			neighbors.add(new Coords(this.x, this.y-1));
		if (env.inForest(this.x,this.y+1))
			neighbors.add(new Coords(this.x, this.y+1));
		return neighbors; 
		
	}
	
	/**
	 * Fonction qui permet d'avoir les voisins d'une case, sans verification de s'ils sont dans la foret
	 * Utilisee pour la generalisation des regles
	 * @return
	 */
	public ArrayList<Coords> neighborsRegles(){
		ArrayList<Coords> neighbors = new ArrayList<Coords>();
		
		neighbors.add(new Coords(this.x-1, this.y));
		neighbors.add(new Coords(this.x+1, this.y));
		neighbors.add(new Coords(this.x, this.y-1));
		neighbors.add(new Coords(this.x, this.y+1));
		
		return neighbors; 
	}
	
	/**
	 * Fonction qui renvoie si la coordonnee est dans la foret
	 * @param env
	 * @return
	 */
	public boolean inForest(Environment env) {
		return env.inForest(this.x, this.y);
	}
}
