package model;

import java.util.ArrayList;

public class Capteur {
	
	private Coords coords; 
	private Case caseActuelle; 
	
	public Capteur() {
		this.coords = new Coords(0,0); 
		this.caseActuelle = null; 
	}

	public Coords getCoords() {
		return coords;
	}

	public void setCoords(Coords coords) {
		this.coords = coords;
	}

	public Case getCaseActuelle() {
		return caseActuelle;
	}

	public void setCaseActuelle(Case caseActuelle) {
		this.caseActuelle = caseActuelle;
	}
	
	/**
	 * Fonction qui renvoie la liste de faits correspondants a l'observation : la case sur laquelle se trouve le hero
	 * @param env
	 * @param agent
	 * @return
	 */
	public ArrayList<Fait> observeEnvironnement(Environment env, Agent agent) {
		ArrayList<Fait> faits = new ArrayList<Fait>(); 
		this.coords.setX(agent.getCoords().getX());
		this.coords.setY(agent.getCoords().getY());
		caseActuelle = env.getForest()[this.coords.getX()][this.coords.getY()]; 

		faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.VISITED));
		
		switch (env.getForest()[this.coords.getX()][this.coords.getY()]) {
			case EMPTY : 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.SAFE)); 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.EMPTY));
				break; 
			case WIND :
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.WIND)); 
				break; 
			case POOP : 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.POOP)); 
				break; 
			case CRACK : 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.CRACK)); 
				break; 
			case MONSTER : 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.MONSTER)); 
				break; 
			case LIGHT : 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.LIGHT));
				break; 
			case WINDANDPOOP :
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.WIND)); 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.POOP));
				break; 
			case MONSTERANDPOOP : 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.MONSTER)); 
				break;
			case MONSTERANDWIND : 
				faits.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.MONSTER)); 
				break;
		}
		
		return faits; 
		
	}
	
	
	
	
	
	
	
	

}
