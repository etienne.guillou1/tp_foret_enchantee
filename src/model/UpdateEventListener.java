package model;

public interface UpdateEventListener {
	void onUpdateEnv();
	void onUpdateAgent();
}