package model;

import java.util.ArrayList;

public class Intention {
	
	private ArrayList<Action> actions; 
	
	public Intention() {
		this.actions = new ArrayList<>(); 
	}

	public ArrayList<Action> getActions() {
		return actions;
	}

	public void setActions(ArrayList<Action> actions) {
		this.actions = actions;
	}
	
	
	
}
