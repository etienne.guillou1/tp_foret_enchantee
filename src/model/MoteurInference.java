package model;

import java.util.ArrayList;

import java.util.Random;

public class MoteurInference {
	
	private ArrayList<Fait> faits; 
	private ArrayList<Regle> regles; 
	private ArrayList<RegleMarquee> reglesMarquees; 
	
	public MoteurInference(ArrayList<Fait> faits) {
		this.faits = faits; 
		this.regles = regles(); 
		this.reglesMarquees = new ArrayList<RegleMarquee>();
	}

	public ArrayList<Fait> getFaits() {
		return faits;
	}

	public void setFaits(ArrayList<Fait> faits) {
		this.faits = faits;
	}

	public ArrayList<Regle> getRegles() {
		return regles;
	}

	public void setRegles(ArrayList<Regle> regles) {
		this.regles = regles;
	}
	
	public ArrayList<RegleMarquee> getReglesMarquees() {
		return reglesMarquees;
	}

	public void setReglesMarquees(ArrayList<RegleMarquee> reglesMarquees) {
		this.reglesMarquees = reglesMarquees;
	}

	public void ajouteFaitsObserves(ArrayList<Fait> faits) {
		for (Fait fait : faits) {
			this.faits.add(fait);
		}
	}
	
	public void init() {
		this.faits.clear();
		this.reglesMarquees.clear();
	}
	
	/**
	 * Fonction qui permet d'ajouter les regles 
	 * @return
	 */
	public ArrayList<Regle> regles() {
		
		ArrayList<Regle> regles = new ArrayList<Regle>(); 
		
		regles.add(ifPoopThenRiskmonster());
		regles.add(ifWindThenRiskcrack());
		regles.add(ifEmptyThenSafe());
		regles.add(cibleIfSafe());
		regles.add(exitIfLight());
		regles.add(tirerIfMonstre());
		regles.add(tirerIfRiskmonstre());
		
		return regles; 
	}
	
	/**
	 * Fonction qui permet de choisir une action a effectuer suivant les faits
	 * @param env
	 * @return
	 */
	public But chooseAction(Environment env) {
		But but = null; 
		int alea = new Random().nextInt(frontiere(env).size()-1); //Au cas ou aucune action trouvee
		System.out.println(alea);
		int cpt = 0; 
		Coords coords = null; 
		
		for (Coords c : frontiere(env)) {
			if (this.faits.contains(new Fait(c, Etat.CIBLESORTIE)))
				return new But(new Coords(c.getX(), c.getY()), ActionBut.CIBLE);
			if (cpt == alea) {
				coords = new Coords(c.getX(), c.getY());
			}
			else if (this.faits.contains(new Fait(c, Etat.CIBLE))) {
				But newBut = new But(new Coords(c.getX(), c.getY()), ActionBut.CIBLE); 
				if (but == null || but.moreImportant(newBut))
					but = newBut;
			}
			else if (this.faits.contains(new Fait(c, Etat.TIRER))) {
				But newBut = new But(new Coords(c.getX(), c.getY()), ActionBut.TIRER); 
				if (but == null)
					but = newBut; 
			}
			cpt++;
			
		}
		
		if (but == null) {
			but = new But(coords, ActionBut.CIBLE);
		}
		
			System.out.println();
			for (Fait f : this.faits)
				System.out.println(f);
			System.out.println();
			for (RegleMarquee r : reglesMarquees)
				System.out.println(r);
			System.out.println();
		
		if (but.getAction() == ActionBut.TIRER) {
			this.faits.remove(new Fait(but.getCoords(), Etat.TIRER));
		}
		else 
			this.faits.remove(new Fait(but.getCoords(), Etat.CIBLE));
		
		return but; 	
	}
	
	/**
	 * Fonction qui execute tout le processus necessaire pour le cycle d'inference
	 * @param env
	 * @param faits
	 * @return
	 */
	public But cycleInference(Environment env, ArrayList<Fait> faits) {
		this.faits.clear();
		this.faits.addAll(faits); 
		runRegles(env); 
		return chooseAction(env); 
	}
	
	/**
	 * Regle : 
	 * Si caca ALORS risque de monstre sur les cases voisines
	 * SI poop(X,Y) ALORS riskmonster(X-1,Y), riskmonster(X+1,Y), riskmonster(X,Y-1), riskmonster(X,Y+1)
	 */
	public Regle ifPoopThenRiskmonster(){
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheurs
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.POOP));
		
		//Faits ressortants
		for (Coords c : coords.neighborsRegles()) 
			ressortants.add(new Fait(new Coords(c.getX(), c.getY()), Etat.RISKMONSTER));
		
		return new Regle(declencheurs, ressortants);
	}
	
	/**
	 * Regle : 
	 * Si vent ALORS risque de crevasses sur les cases voisines
	 * SI wind(X,Y) ALORS riskcrack(X-1,Y), riskcrack(X+1,Y), riskcrack(X,Y-1), riskcrack(X,Y+1)
	 */
	public Regle ifWindThenRiskcrack(){
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheurs
		Fait declencheur = new Fait(new Coords(coords.getX(), coords.getY()), Etat.WIND);
		declencheurs.add(declencheur);
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.VISITED));
		for (Coords c : coords.neighborsRegles()) 
			declencheurs.add(new Fait(new Coords(c.getX(), c.getY()), Etat.VISITED, false));
		
		//Faits ressortants
		for (Coords c : coords.neighborsRegles()) 
			ressortants.add(new Fait(new Coords(c.getX(), c.getY()), Etat.RISKCRACK));
		
		return new Regle(declencheurs, ressortants);
	}
	
	/**
	 * Regle : 
	 * Si vide ALORS safe sur les cases voisines
	 * SI empty(X,Y) ALORS safe(X-1,Y), safe(X+1,Y), safe(X,Y-1), safe(X,Y+1)
	 */
	public Regle ifEmptyThenSafe(){
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheurs
		Fait declencheur = new Fait(new Coords(coords.getX(), coords.getY()), Etat.EMPTY);
		declencheurs.add(declencheur);
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.VISITED));
		
		//Faits ressortants
		for (Coords c : coords.neighborsRegles()) 
			ressortants.add(new Fait(new Coords(c.getX(), c.getY()), Etat.SAFE));
		
		return new Regle(declencheurs, ressortants);
	}
	
	/**
	 * Regle : 
	 * Si nomonster ALORS safe
	 * SI nomonster(X,Y) ALORS safe(X,Y)
	 */
	public Regle safeIfNomonster() {
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheur
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.MONSTER, false));
		
		//Faits ressortants
		ressortants.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.SAFE));
		
		return new Regle(declencheurs, ressortants);
		
	}
	
	/**
	 * Regle : 
	 * Si safe ALORS cible
	 * SI safe(X,Y) ALORS cible(X,Y)
	 */
	public Regle cibleIfSafe() {
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheur
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.SAFE));
		
		//Faits ressortants
		ressortants.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.CIBLE));
		
		return new Regle(declencheurs, ressortants);
		
	}
	/**
	 * Regle : 
	 * Si lumiere ALORS ciblesortie
	 * SI light(X,Y) ALORS ciblesortie(X,Y)
	 */
	public Regle exitIfLight() {
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheur
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.LIGHT));
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.VISITED));
		
		//Faits ressortants
		ressortants.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.CIBLESORTIE));
		
		return new Regle(declencheurs, ressortants);
	}
	
	/**
	 * Regle : 
	 * Si risquemonstre ALORS tirer
	 * SI riskmonstre(X,Y) ALORS tirer(X,Y)
	 */
	public Regle tirerIfRiskmonstre() {
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheur
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.RISKMONSTER));
		
		//Faits ressortants
		ressortants.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.TIRER));
		
		return new Regle(declencheurs, ressortants);
		
	}
	
	/**
	 * Regle : 
	 * Si monstre ALORS tirer
	 * SI monstre(X,Y) ALORS tirer(X,Y)
	 */
	public Regle tirerIfMonstre() {
		
		ArrayList<Fait> declencheurs = new ArrayList<Fait>(); 
		ArrayList<Fait> ressortants = new ArrayList<Fait>(); 
		Coords coords = new Coords(0,0); 
		
		//Faits declencheur
		declencheurs.add(new Fait(new Coords(coords.getX(), coords.getY()), Etat.MONSTER));
		
		//Faits ressortants
		ressortants.add(new Fait(new Coords(coords.getX() , coords.getY()), Etat.TIRER));
		
		return new Regle(declencheurs, ressortants);
		
	}
	
	
	
	public void runRegles(Environment env) {
		for (int i=0; i<env.getLevel(); i++) {
			for (int j=0; j<env.getLevel(); j++) {
				Coords c = new Coords(i,j);
				for (Regle regle : this.regles) {
					if (regle.isRespect(faits,c) && !reglesMarquees.contains(new RegleMarquee(regle,c))) {
						this.faits.addAll(regle.faitsResultants(c, env));
						this.reglesMarquees.add(new RegleMarquee(regle, c));
					}
				}
			}
		}
	}

	/**
	 * Fonction permettant d'obtenir la frontiere (les cases non visitees et qui sont proches des cases visitees) 
	 * @param env
	 * @return
	 */
	public ArrayList<Coords> frontiere(Environment env){
		ArrayList<Coords> frontiere = new ArrayList<Coords>(); 
		for (Fait f : this.faits) {
			if (f.getEtat() == Etat.VISITED) {
				for (Coords c : f.getCoords().neighbors(env)) {
					if (!this.faits.contains(new Fait(c, Etat.VISITED)))
						frontiere.add(new Coords(c.getX(), c.getY()));
				}
					
			}	
		}
		return frontiere; 
	}
	
}
