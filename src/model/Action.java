package model;

/**
 * Type enumere qui correspond aux actions effectuees par le hero
 */

public enum Action {
	GAUCHE, DROITE, HAUT, BAS, SORTIR, TIRERGAUCHE, TIRERDROITE, TIRERBAS, TIRERHAUT
}
