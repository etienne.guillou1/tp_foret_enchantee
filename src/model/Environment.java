package model;

import java.util.Random;

public class Environment {
	
	private Case[][] forest; 
	private int level; 
	private UpdateEventListener updateListener;
	
	public Environment(int level) {
		this.level = level;
		this.forest = new Case[this.level][this.level]; 
		
		for (int i=0; i<level; i++) {
			for (int j=0; j<level; j++)
				this.forest[i][j] = Case.EMPTY;
		}
		
		generateForest(); 
	}


	public Case[][] getForest() {
		return forest;
	}

	public void setForest(Case[][] forest) {
		this.forest = forest;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	/**
	 * Fonction qui met a jour la foret, lorsqu'on change de niveau
	 */
	public void updateForest() {
		this.forest = new Case[this.level][this.level]; 

		for (int i=0; i<level; i++) {
			for (int j=0; j<level; j++) {
				this.forest[i][j] = Case.EMPTY;
			}
		}
		generateForest();
		update();
	}
	
	/**
	 * Fonction qui genere le portail (et donc la lumiere), les monstres (et les cacas de monstres), 
	 * ansi que les crevasses (et le vent).
	 */
	public void generateForest() {
		//Generation du portail 
		int aleaPortail = new Random().nextInt((level*level)-1);
		
		int cpt = 0; 
		
		for (int i=0; i<this.level; i++) {
			for (int j=0; j<this.level; j++) {
				if (i != 0 || j!= 0) { //On ne place rien sur la premiere case, car case de depart du hero
					if (cpt == aleaPortail) {
						this.forest[i][j] = Case.LIGHT; //Portail de sortie
					}
					
					else { //Pour chaque case on choisit si il y a un monstre, une crevasse ou rien 
						int alea = new Random().nextInt(10);
						if (alea == 1)
							this.forest[i][j] = Case.MONSTER; 
						else if (alea == 2)
							this.forest[i][j] = Case.CRACK;
						else
							this.forest[i][j] = Case.EMPTY; 
					}
					cpt++; 
				}
			}
		}
		
		addPoopWind(); 
	}
	
	/**
	 * Fonction qui permet de boucler sur la foret, et qui lance la fonction qui permet d'ajouter le caca ou le vent suivant le cas
	 */
	public void addPoopWind() {
		for (int i=0; i<this.level; i++) {
			for (int j=0; j<this.level; j++) {
				if (this.forest[i][j] == Case.MONSTER)
					generatePoop(i, j); 
				else if (this.forest[i][j] == Case.CRACK)
					generateWind(i, j); 
			}
		}
	}
	
	/**
	 * Fonction qui permet de choisir les cases o� ajouter le caca
	 * @param i
	 * @param j
	 */
	public void generatePoop(int i, int j) {
		Coords coords = new Coords(i,j); 
		for (Coords c : coords.neighbors(this)) {
			addPoop(c.getX(), c.getY());
		}
	}
	
	/**
	 * Fonction qui permet de choisir ou ajouter le vent
	 * @param i
	 * @param j
	 */
	public void generateWind(int i, int j) {
		Coords coords = new Coords(i,j); 
		for (Coords c : coords.neighbors(this)) {
			addWind(c.getX(), c.getY());
		} 
	}
	
	/**
	 * Fonction qui ajoute le caca
	 * @param i
	 * @param j
	 */
	public void addPoop(int i, int j) {
		if (this.forest[i][j] == Case.EMPTY) 
			this.forest[i][j] = Case.POOP; 
		else if (this.forest[i][j] == Case.WIND)
			this.forest[i][j] = Case.WINDANDPOOP;
		else if (this.forest[i][j] == Case.MONSTER)
			this.forest[i][j] = Case.MONSTERANDPOOP;
	}
	
	/**
	 * Fonction qui ajoute le vent
	 * @param i
	 * @param j
	 */
	public void addWind(int i, int j) {
		if (this.forest[i][j] == Case.EMPTY) 
			this.forest[i][j] = Case.WIND; 
		else if (this.forest[i][j] == Case.POOP)
			this.forest[i][j] = Case.WINDANDPOOP;
		else if (this.forest[i][j] == Case.MONSTER)
			this.forest[i][j] = Case.MONSTERANDWIND;
	}

	/**
	 * Fonction qui permet de savoir si une coordonnee est dans la foret ou pas 
	 * @param i
	 * @param j
	 * @return
	 */
	public boolean inForest(int i, int j) {
		return (i>=0 && j>=0 && i<this.level && j<this.level);
	}

	/**
	 * Fonction qui permet de mettre a jour l'interface graphique
	 */
	public void update() {
		if (updateListener != null) 
			updateListener.onUpdateEnv();
	}
	
	/**
	 * Mecanisme permettant de faire communiquer l'interface graphique avec l'environnement
	 * @param updateListener
	 */
	public void setUpdateEventListener(UpdateEventListener updateListener) {
		this.updateListener = updateListener;
	}
	
	public void changeCase(Coords coords, Case newCase) {
		this.forest[coords.getX()][coords.getY()] = newCase; 
	}
}
