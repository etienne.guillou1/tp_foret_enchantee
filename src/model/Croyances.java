package model;

import java.util.ArrayList;

public class Croyances {
	
	private ArrayList<Fait> faits; 
	
	public Croyances() {
		this.faits = new ArrayList<Fait>(); 
	}

	public ArrayList<Fait> getFaits() {
		return faits;
	}

	public void setFaits(ArrayList<Fait> faits) {
		this.faits = faits;
	}
	
	public void ajouteFaits(ArrayList<Fait> list) {
		for (Fait fait : list) {
			if (!this.faits.contains(fait))
				faits.add(fait);
		}
	}

}
