package model;

public class RegleMarquee {
	
	private Regle regle; 
	private Coords coords;
	
	public RegleMarquee(Regle regle, Coords coords) {
		this.regle = regle; 
		this.coords = coords; 
	}

	public Regle getRegle() {
		return regle;
	}

	public void setRegle(Regle regle) {
		this.regle = regle;
	}

	public Coords getCoords() {
		return coords;
	}

	public void setCoords(Coords coords) {
		this.coords = coords;
	}

	@Override
	public String toString() {
		return "RegleMarquee [regle=" + regle + ", coords=" + coords + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coords == null) ? 0 : coords.hashCode());
		result = prime * result + ((regle == null) ? 0 : regle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		RegleMarquee other = (RegleMarquee) obj;
		if (!this.regle.equals(other.getRegle()))
			return false; 
		if (!this.coords.equals(other.getCoords()))
			return false; 
		return true; 
		
	}
	
}
