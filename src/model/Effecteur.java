package model;

public class Effecteur {
	
	public Coords gauche(Coords depart) {
		return new Coords(depart.getX(), depart.getY()-1); 
	}
	
	public Coords droite(Coords depart) {
		return new Coords(depart.getX(), depart.getY()+1); 
	}
	
	public Coords haut(Coords depart) {
		return new Coords(depart.getX()-1, depart.getY()); 
	}
	
	public Coords bas(Coords depart) {
		return new Coords(depart.getX()+1, depart.getY()); 
	}
	
	/**
	 * L'agent a trouv� la sortie, on passe au niveau suivant de la foret
	 * @param env
	 */
	public void sortir(Environment env) {
		 env.setLevel(env.getLevel()+1);
		 env.updateForest();
	}
	
	public void tirer(Environment env, Coords depart, Action direction) {
		Coords coords = new Coords(depart.getX(), depart.getY());
		//On commence par trouver les coordonnees de la case sur laquelle on tire
		switch (direction) {
			case HAUT : 
				coords.setX(coords.getX()-1);
				break;
			case BAS : 
				coords.setX(coords.getX()+1);
				break;
			case GAUCHE : 
				coords.setY(coords.getY()-1);
				break;
			case DROITE : 
				coords.setY(coords.getY()+1);
				break;	
		}
		
		//Puis on modifie la case de l'environnement
		if (env.getForest()[coords.getX()][coords.getY()] == Case.MONSTER) {
			env.changeCase(coords, Case.EMPTY); 
		}
		else if (env.getForest()[coords.getX()][coords.getY()] == Case.MONSTERANDPOOP) {
			env.changeCase(coords, Case.POOP); 
		}
		else if (env.getForest()[coords.getX()][coords.getY()] == Case.MONSTERANDWIND) {
			env.changeCase(coords, Case.WIND); 
		}
	}

}
