package model;

public class Fait {
	
	private Coords coords; 
	private Etat etat; 
	private boolean vrai; 
	
	public Fait(Coords coords, Etat etat) {
		this.coords = coords; 
		this.etat = etat; 
		this.vrai = true; 
	}
	
	public Fait(Coords coords, Etat etat, Boolean vrai) {
		this.coords = coords; 
		this.etat = etat; 
		this.vrai = vrai; 
	}

	public Coords getCoords() {
		return coords;
	}

	public void setCoords(Coords coords) {
		this.coords = coords;
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}

	public boolean isVrai() {
		return vrai;
	}

	public void setValue(boolean vrai) {
		this.vrai = vrai;
	}

	@Override
	public String toString() {
		return "Fait [coords=" + coords + ", etat=" + etat + ", vrai=" + vrai + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coords == null) ? 0 : coords.hashCode());
		result = prime * result + ((etat == null) ? 0 : etat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Fait other = (Fait) obj;
		if (this.coords.equals(other.getCoords()) && this.etat.equals(other.getEtat())) {
			if (other.isVrai() && this.vrai)
				return true; 
			else if (!other.isVrai() && !this.vrai)
				return true; 
			return false; 
		}
			
		return false; 
	}
	
	
	
	
}
