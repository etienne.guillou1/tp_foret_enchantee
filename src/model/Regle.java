package model;

import java.util.ArrayList;

public class Regle {
	
	private ArrayList<Fait> declencheurs; 
	private ArrayList<Fait> resultants;
	
	public Regle(ArrayList<Fait> declencheurs, ArrayList<Fait> resultants) {
		this.declencheurs = declencheurs; 
		this.resultants = resultants;
	}

	public ArrayList<Fait> getDeclencheurs() {
		return declencheurs;
	}

	public void setDeclencheurs(ArrayList<Fait> declencheurs) {
		this.declencheurs = declencheurs;
	}

	public ArrayList<Fait> getResultants() {
		return resultants;
	}

	public void setResultants(ArrayList<Fait> resultants) {
		this.resultants = resultants;
	}
	
	/**
	 * Fonction qui verifie si la regle est verifiee
	 * @param faits
	 * @param coords
	 * @return
	 */
	public boolean isRespect(ArrayList<Fait> faits, Coords coords) {
		
		for (Fait f : declencheurs) {
			int x = f.getCoords().getX(); int y = f.getCoords().getY();
			Fait fait = new Fait(new Coords(x + coords.getX(), y + coords.getY()), f.getEtat());
			if (f.isVrai() && !faits.contains(fait))
				return false;
			else if (!f.isVrai() && faits.contains(fait))
				return false;
		}
		return true; 
	}
	
	/**
	 * Fonction qui renvoie les faits resultants de la regle
	 * @param coords
	 * @param env
	 * @return
	 */
	public ArrayList<Fait> faitsResultants(Coords coords, Environment env){
		ArrayList<Fait> faits = new ArrayList<Fait>(); 
		for (Fait f : this.resultants) {
			Coords c = new Coords(f.getCoords().getX() + coords.getX(),  f.getCoords().getY() + coords.getY());
			if (env.inForest(c.getX(), c.getY()))
				faits.add(new Fait(c, f.getEtat()));
		}
		return faits; 
	}

	@Override
	public String toString() {
		return "Regle [declencheurs=" + declencheurs + ", resultants=" + resultants + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((declencheurs == null) ? 0 : declencheurs.hashCode());
		result = prime * result + ((resultants == null) ? 0 : resultants.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Regle other = (Regle) obj;
		for (Fait f : other.getDeclencheurs()) {
			if (!declencheurs.contains(f))
				return false;
		}
		for (Fait f : other.getResultants()) {
			if (!resultants.contains(f))
				return false;
		}
		return true; 
			
	}
	
}
