package model;

/**
 * Classe permettant de definir un but : une action, et les coordonnees correspondantes
 */

public class But {
	
	private Coords coords; 
	private ActionBut action; 
	
	public But(Coords coords, ActionBut action) {
		this.coords = coords; 
		this.action = action; 
	}

	public Coords getCoords() {
		return coords;
	}

	public void setCoords(Coords coords) {
		this.coords = coords;
	}

	public ActionBut getAction() {
		return action;
	}

	public void setAction(ActionBut action) {
		this.action = action;
	}
	
	/**
	 * Donne un ordre de priorite aux buts
	 * @param but
	 * @return
	 */
	public boolean moreImportant(But but) {
		if (this.action == ActionBut.TIRER && but.getAction() == ActionBut.CIBLE)
			return true; 
		return false; 
	}

	@Override
	public String toString() {
		return "But [coords=" + coords + ", action=" + action + "]";
	}
	
	

}
