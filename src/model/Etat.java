package model;
/**
 * Type enumere qui prend en compte les differents etats possibles (pour les regles)
 */
public enum Etat {
	VISITED, SAFE, EMPTY, CRACK, MONSTER, WIND, POOP, LIGHT, RISKMONSTER, RISKCRACK, CIBLE, TIRER, CIBLESORTIE
}
