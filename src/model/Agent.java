package model;

import java.util.ArrayList;

import view.Fenetre;

public class Agent {
	
	private Coords coords; 
	
	//Etat mental de l'agent
	private Intention intention; 
	private Croyances croyances; 
	private Desir desir; 
	
	private Capteur capteur; 
	private Effecteur effecteur; 
	private MoteurInference moteurInference; 
	
	private Environment environnement;
	private UpdateEventListener updateListener;	
	
	public Agent(Environment environnement) {
		this.coords = new Coords(0,0); //Le hero est place par convention en haut a gauche de la foret
		
		this.intention = new Intention();
		this.croyances = new Croyances();
		this.desir = new Desir();
		
		this.capteur = new Capteur(); 
		this.effecteur = new Effecteur(); 
		this.moteurInference = new MoteurInference(new ArrayList<Fait>());
		
		this.environnement = environnement; 
	}
	
	public MoteurInference getMoteurInference() {
		return moteurInference;
	}

	public void setMoteurInference(MoteurInference moteurInference) {
		this.moteurInference = moteurInference;
	}

	public Environment getEnvironnement() {
		return environnement;
	}

	public void setEnvironnement(Environment environnement) {
		this.environnement = environnement;
	}

	public UpdateEventListener getUpdateListener() {
		return updateListener;
	}

	public void setUpdateListener(UpdateEventListener updateListener) {
		this.updateListener = updateListener;
	}

	public Coords getCoords() {
		return coords;
	}

	public void setCoords(Coords coords) {
		this.coords = coords;
	}

	public Intention getIntention() {
		return intention;
	}

	public void setIntention(Intention intention) {
		this.intention = intention;
	}

	public Croyances getCroyances() {
		return croyances;
	}

	public void setCroyances(Croyances croyances) {
		this.croyances = croyances;
	}

	public Desir getDesir() {
		return desir;
	}

	public void setDesir(Desir desir) {
		this.desir = desir;
	}

	public Capteur getCapteur() {
		return capteur;
	}

	public void setCapteur(Capteur capteur) {
		this.capteur = capteur;
	}

	public Effecteur getEffecteur() {
		return effecteur;
	}

	public void setEffecteur(Effecteur effecteur) {
		this.effecteur = effecteur;
	}
	
	public void waitBeforeAction(int timeInMilliseconds){
		try {
			Thread.sleep(timeInMilliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lorsque l'agent perd, il recommence a la case de depart
	 */
	public void restart() {
		this.coords = new Coords(0, 0);
	}
	
	//Fonction a mettre sur le bouton (je pense)
	/**
	 * Fonction qui permet de trouver un but et le deplacement necessaire pour atteindre ce but
	 * C'est la fonction qu'on actionne avec le bouton
	 */
	public void jouer() {
		//On commence par observer l'environnement, et on ajoute les nouvelles connaissances aux croyances
		this.croyances.ajouteFaits(this.capteur.observeEnvironnement(this.environnement, this));
		
		//Si on est sur une case ou il y a de la lumiere --> on sort
		if (this.croyances.getFaits().contains(new Fait(this.coords, Etat.LIGHT))) {
			this.activeEffecteurs(Action.SORTIR);
			return; 
		}
			
		
		//Si on est sur une case ou il y a un monstre ou une crevasse --> on meurt
		//Le hero repart donc de son point de depart, mais garde ses connaissances
		if (isDead()) {
			System.out.println("DEAD");
			this.coords = new Coords(0,0);
			update();
			return;
		}

		boolean did = false; //Booleen qui permet de savoir qu'on a effectue les actions (utile pour le while)
//		System.out.println("\nPOSITION : " + this.coords);
//		Fenetre.displayForest(this.environnement);
		
		while (!did) {
			if (this.desir.getBut() != null) {
				if (!this.intention.getActions().isEmpty()) { // L'agent a un but mais pas d'action
					did = justDoIt();
//					System.out.println(this.coords);
				}
				else { // On definit les actions de l'agent
//					System.out.println("{GETPLAN}");
					this.intention.setActions(getPlan(this.desir.getBut())); 
				}
			}
			else { //on choisit le but grace au moteur d'inference
				But but = this.moteurInference.cycleInference(this.environnement, this.croyances.getFaits()); 
//				System.out.println("BUT = " + but);
				this.desir.setBut(but); //On met a jour le but du hero
				updateFait(this.moteurInference.getFaits()); //on met a jour les croyances du hero
			}
		}
	}
	
	/**
	 * Fonction qui effectue les differentes actions et qui remet a zero la liste des intentions et le desir de l'agent
	 * @return
	 */
	public boolean justDoIt() { 
		
		for (Action action : this.intention.getActions()) {
//			waitBeforeAction(200);
			this.activeEffecteurs(action);
			update();
		}
		
		this.intention.setActions(new ArrayList<Action>());
		this.desir.setBut(null);
		return true; 
	}

	/**
	 * Fonction qui permet d'obtenir la liste des cases par lesquelles le hero doit passer pour aller a la case cible.
	 * Elle permet d'obtenir le chemin de cases qui sont "sures"
	 * @param position
	 * @param cible
	 * @param plan
	 * @return
	 */
	public ArrayList<Coords> explore(Coords position, Coords cible, ArrayList<Coords> plan){
		if (position.getX() == cible.getX() && position.getY() == cible.getY()) {
			System.out.println("Exploration termin�e : " + position + " equals " + cible);
			return plan; 
		}
		for (Coords c : position.neighbors(this.environnement)){
			if((this.croyances.getFaits().contains(new Fait(c, Etat.SAFE)) && this.croyances.getFaits().contains(new Fait(c, Etat.VISITED)) && !plan.contains(c)) || (c.getX() == cible.getX() && c.getY() == cible.getY())) { // Si le voisin est SAFE
				plan.add(c);
				ArrayList<Coords> resultat = explore(c, cible, plan);
				if (resultat == null)
					plan.remove(c);
				else 
					return resultat; 
			}
		}
		return null;
	}
	
	/**
	 * Fonction qui permet d'avoir un plan d'action, suivant le but du hero (obtenu au prealable par le moteur d'inference
	 */
	public ArrayList<Action> getPlan(But but){
//		System.out.println(but);
		Action action;
		ArrayList<Coords> chemin = new ArrayList<>();
		
		// Creation des trois variables pour l'exploration
		Coords cible = but.getCoords();
		Coords position = this.coords;
		ArrayList<Coords> cheminInitial = new ArrayList<>();
		
		// On lance l'exploration
		if(!position.neighbors(environnement).contains(but.getCoords()))
			chemin = explore(position, cible,  cheminInitial);
		else
			chemin.add(but.getCoords());
		
		if (chemin == null)
			return null; 
//		System.out.print("\nCHEMIN ---> ");
//		for (Coords c : chemin) {
//			System.out.print(c.toString() + " > ");
//		}
//		System.out.println();
		ArrayList<Action> plan = new ArrayList<Action>();
		
		for (Coords c : chemin) {
			plan.add(getAction(position, c));
			position = new Coords(c.getX(), c.getY());
		}
		if (but.getAction() == ActionBut.TIRER) {
			action = plan.remove(plan.size()-1);
			switch (action) {
				case HAUT : 
					plan.add(Action.TIRERHAUT); 
					break;
				case BAS : 
					plan.add(Action.TIRERBAS); 
					break;
				case GAUCHE : 
					plan.add(Action.TIRERGAUCHE); 
					break;
				case DROITE : 
					plan.add(Action.TIRERDROITE); 
					break;
			}
		}
		
		return plan; 
	}
	
	/**
	 * Fonction qui permet d'obtenir la direction, suivant une case de depart et une case d'arrivee
	 * @param depart
	 * @param arrivee
	 * @return
	 */
	public Action getAction(Coords depart, Coords arrivee) {
		if (depart.getX() > arrivee.getX())
			return Action.HAUT; 
		if (depart.getX() < arrivee.getX())
			return Action.BAS; 
		if (depart.getY() > arrivee.getY())
			return Action.GAUCHE; 
		return Action.DROITE;
	}
	
	public void run() {
		System.out.println("Thread Agent is running");
	}
	
	/**
	 * Fonction permettant de savoir si on est sur une case monstre ou crevasse, et que du coup le hero meurt
	 * @return
	 */
	public boolean isDead() {
		if (this.croyances.getFaits().contains(new Fait(this.coords, Etat.CRACK)))
				return true; 
		if (this.croyances.getFaits().contains(new Fait(this.coords, Etat.MONSTER)))
			return true; 
		return false; 
	}
	
	/**
	 * Fonction permettant de mettre a jour les croyances de l'agent, suivant les faits renvoyes par le moteur d'inference
	 * @param faits
	 */
	public void updateFait(ArrayList<Fait> faits) {
		this.croyances.getFaits().clear(); 
		this.croyances.getFaits().addAll(faits);
	}
	
	/**
	 * Fonction qui active l'effecteur en fonction de l'action choisie
	 * En cas de tir, elle met egalement a jour les croyances du hero (pas de risque de monstre, et monstre a false sur case visee) 
	 * @param action
	 * @return
	 */
	public boolean activeEffecteurs(Action action) {
		System.out.println(action);
		switch (action) {
			case GAUCHE : 
				this.coords = this.effecteur.gauche(this.coords); 
				break; 
			case DROITE:
				this.coords = this.effecteur.droite(this.coords); 
				break; 
			case BAS : 
				this.coords = this.effecteur.bas(this.coords); 
				break;
			case HAUT : 
				this.coords = this.effecteur.haut(this.coords); 
				break;
			case TIRERHAUT : 
				this.effecteur.tirer(this.environnement, this.coords, Action.HAUT);
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX()-1, this.coords.getY()), Etat.MONSTER));
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX()-1, this.coords.getY()), Etat.RISKMONSTER));
				this.croyances.getFaits().add(new Fait(new Coords(this.coords.getX()-1, this.coords.getY()), Etat.SAFE));
				break; 
			case TIRERBAS :
				this.effecteur.tirer(this.environnement, this.coords, Action.BAS);
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX()+1, this.coords.getY()), Etat.MONSTER));
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX()+1, this.coords.getY()), Etat.RISKMONSTER));
				this.croyances.getFaits().add(new Fait(new Coords(this.coords.getX()+1, this.coords.getY()), Etat.SAFE));
				break; 
			case TIRERGAUCHE : 
				this.effecteur.tirer(this.environnement, this.coords, Action.GAUCHE);
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX(), this.coords.getY()-1), Etat.MONSTER));
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX(), this.coords.getY()-1), Etat.RISKMONSTER));
				this.croyances.getFaits().add(new Fait(new Coords(this.coords.getX(), this.coords.getY()-1), Etat.SAFE));
				break; 
			case TIRERDROITE : 
				this.effecteur.tirer(this.environnement, this.coords, Action.DROITE);
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX(), this.coords.getY()+1), Etat.MONSTER));
				this.croyances.getFaits().remove(new Fait(new Coords(this.coords.getX(), this.coords.getY()+1), Etat.RISKMONSTER));
				this.croyances.getFaits().add(new Fait(new Coords(this.coords.getX(), this.coords.getY()+1), Etat.SAFE));
				break; 
			case SORTIR : 
				this.coords = new Coords(0,0);
				this.croyances.getFaits().clear();
				this.moteurInference.init();
				this.effecteur.sortir(this.environnement); 
				
				return true; 
			default : 
				System.out.println("No action found");
		}
		return false;
	}
	
	/**
	 * Fonction qui permet de mettre a jour l'interface graphique
	 */
	public void update() {
		if (updateListener != null) 
			updateListener.onUpdateAgent();
	}
	
	/**
	 * Mecanisme permettant de faire communiquer l'interface graphique avec l'environnement
	 * @param updateListener
	 */
	public void setUpdateEventListener(UpdateEventListener updateListener) {
		this.updateListener = updateListener;
	}
	
}
