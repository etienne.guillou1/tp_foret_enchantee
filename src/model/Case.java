package model;
/**
 * Type enumere qui reprend les differentes compositions possibles des cases
 */
public enum Case {
	EMPTY, WIND, POOP, CRACK, MONSTER, LIGHT, WINDANDPOOP, MONSTERANDPOOP, MONSTERANDWIND
}
